# Bank App

A SwiftUI demo project.

<p>
'<t>HidableTabView</t>' package is used for hiding and showing TabView wwith animation. <br>
'<t>HideableTabView</t>' framework can be added to your project as a package thorught SPM. <br>
</p>

<p>
To get more info about the package:
<ul>
    <li> https://gitlab.com/AliMertOzhayta/hidabletabview-swiftui/ </li>
</ul>
</p>
<br>



<table>
<thead>
   <tr>
        <th colspan="5"><div align="center">Screenshots</div></th>
  </tr>
</thead>
<tbody>
   <tr>
        <td>
         <img src="screenshots/dark1.png" width="200"> 
        </td>
        <td>
          <img src="screenshots/dark2.png" width="200">
        </td>
        <td>
         <img src="screenshots/dark3.png" width="200">
       </td>
       <td>
         <img src="screenshots/dark4.png" width="200">
     </td>
      <td>
         <img src="screenshots/dark5.png" width="200">
     </td>
  </tr>
  <tr>
        <td>
         <img src="screenshots/light1.png" width="200"> 
        </td>
        <td>
          <img src="screenshots/light2.png" width="200">
        </td>
        <td>
         <img src="screenshots/light3.png" width="200">
     </td>
      <td>
         <img src="screenshots/light4.png" width="200">
     </td>
      <td>
         <img src="screenshots/light5.png" width="200">
     </td>
  </tr>
  <tr> 
     <th colspan="5">
     <div align="center">
        <b>Demo</b>
</div>
</th>
  </tr>
  <tr>
     <td colspan="5"> ![Demo Link](screenshots/demo.mp4) </td>
  </tr>
</tbody>
</table>
